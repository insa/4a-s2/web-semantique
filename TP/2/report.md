# Question 1
**Quelles sont les pièces contenues dans la maison ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX maison: <http://www.lamaisondumeurtre.fr/instances#LaMaisonDuMeurtre>

SELECT ?Piece WHERE
{
    maison: :maisonContientPiece ?Piece.
}
LIMIT 10
```

## Résultat
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Piece"/>
  </head>
  <results>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Salon</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Bureau</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Chambre</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Couloir</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Cuisine</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#SalleDeBain</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Toilette</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#SalleAManger</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Hall</uri>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 2
** Combien y a-t-il de pièces dans la maison ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX maison: <http://www.lamaisondumeurtre.fr/instances#LaMaisonDuMeurtre>

SELECT (count(?Piece) as ?NombrePieces) WHERE
{
    maison: :maisonContientPiece ?Piece.
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="NombrePieces"/>
  </head>
  <results>
    <result>
      <binding name="NombrePieces">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">9</literal>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 3
**Combien y a-t-il de personnes dans la maison ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX maison: <http://www.lamaisondumeurtre.fr/instances#LaMaisonDuMeurtre>

SELECT (count(?Personne) as ?NombrePersonnes) WHERE
{
  	maison: :maisonContientPiece ?Piece.
  	{
      	{?Personne :personneDansPiece ?Piece} UNION
      	{?Piece :pieceContientPersonne ?Personne}
    }
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="NombrePersonnes"/>
  </head>
  <results>
    <result>
      <binding name="NombrePersonnes">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">10</literal>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 4
**Qui est la victime ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?Personne WHERE
{
	?Personne :estVivant false.
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Personne"/>
  </head>
  <results>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#PattChance</uri>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 5
**Donnez la liste des suspects**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?Personne WHERE
{
	?Personne :estVivant true.
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Personne"/>
  </head>
  <results>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#AldoBermann</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#ArmandChabalet</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#AubinGladeche</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#EmmaNiolia</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#GladisTileri</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#GustaveHarie</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#GuyTarsaich</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#JackPot</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#LucieNuzyte</uri>
      </binding>
    </result>
    <result>
      <binding name="Personne">
        <uri>http://www.lamaisondumeurtre.fr/instances#PaulOchon</uri>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 6
**Dans quelle pièce se situe la victime ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?Piece WHERE
{
	?Victime :estVivant false.
  	{
      	{?Victime :personneDansPiece ?Piece} UNION
      	{?Piece :pieceContientPersonne ?Victime}
    }
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Piece"/>
  </head>
  <results>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Chambre</uri>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 7
**Y a-t-il d'autres personnes dans cette pièce ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

ASK
{
	?Victime :estVivant false.
  	{
      	{?Victime :personneDansPiece ?Piece} UNION
      	{?Piece :pieceContientPersonne ?Victime}
    }

  	{
      	{?Personne :personneDansPiece ?Piece} UNION
      	{?Piece :pieceContientPersonne ?Personne}
    }
  	FILTER (?Personne != ?Victime)
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
  </head>
  <boolean>false</boolean>
</sparql>
```

# Question 8
**Listez les personnes dans chaque pièce.**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?Piece (GROUP_CONCAT(?Personne) AS ?PersonnesPresentes) WHERE
{
  	{
      	{?Personne :personneDansPiece ?Piece} UNION
      	{?Piece :pieceContientPersonne ?Personne}
    }
}

GROUP BY ?Piece
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Piece"/>
    <variable name="PersonnesPresentes"/>
  </head>
  <results>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Bureau</uri>
      </binding>
      <binding name="PersonnesPresentes">
        <literal>http://www.lamaisondumeurtre.fr/instances#AubinGladeche http://www.lamaisondumeurtre.fr/instances#VanessaLami</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Chambre</uri>
      </binding>
      <binding name="PersonnesPresentes">
        <literal>http://www.lamaisondumeurtre.fr/instances#PattChance</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#SalleDeBain</uri>
      </binding>
      <binding name="PersonnesPresentes">
        <literal>http://www.lamaisondumeurtre.fr/instances#GladisTileri</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Salon</uri>
      </binding>
      <binding name="PersonnesPresentes">
        <literal>http://www.lamaisondumeurtre.fr/instances#ArmandChabalet http://www.lamaisondumeurtre.fr/instances#LucieNuzyte</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Toilette</uri>
      </binding>
      <binding name="PersonnesPresentes">
        <literal>http://www.lamaisondumeurtre.fr/instances#EmmaNiolia</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#SalleAManger</uri>
      </binding>
      <binding name="PersonnesPresentes">
        <literal>http://www.lamaisondumeurtre.fr/instances#GuyTarsaich http://www.lamaisondumeurtre.fr/instances#PaulOchon</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Hall</uri>
      </binding>
      <binding name="PersonnesPresentes">
        <literal>http://www.lamaisondumeurtre.fr/instances#SophieStulle</literal>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 9
**Combien y a-t-il de personnes dans chaque pièce (contenant au moins une personne) ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?Piece (COUNT(?Personne) AS ?NombrePersonnes) WHERE
{
  	{
      	{?Personne :personneDansPiece ?Piece} UNION
      	{?Piece :pieceContientPersonne ?Personne}
    }
}

GROUP BY ?Piece
HAVING (?NombrePersonnes > 0)
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Piece"/>
    <variable name="NombrePersonnes"/>
  </head>
  <results>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Bureau</uri>
      </binding>
      <binding name="NombrePersonnes">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">2</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Chambre</uri>
      </binding>
      <binding name="NombrePersonnes">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">1</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#SalleDeBain</uri>
      </binding>
      <binding name="NombrePersonnes">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">1</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Salon</uri>
      </binding>
      <binding name="NombrePersonnes">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">2</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Toilette</uri>
      </binding>
      <binding name="NombrePersonnes">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">1</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#SalleAManger</uri>
      </binding>
      <binding name="NombrePersonnes">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">2</literal>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Hall</uri>
      </binding>
      <binding name="NombrePersonnes">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">1</literal>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 10
**Quelle(s) pièce(s) contien-nen-t au moins deux personnes ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?Piece WHERE
{
	SELECT ?Piece (COUNT(?Personne) AS ?NombrePersonnes) WHERE
	{
  		{
      		{?Personne :personneDansPiece ?Piece} UNION
      		{?Piece :pieceContientPersonne ?Personne}
    	}
	}

	GROUP BY ?Piece
	HAVING (?NombrePersonnes >= 2)
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Piece"/>
  </head>
  <results>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Bureau</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#Salon</uri>
      </binding>
    </result>
    <result>
      <binding name="Piece">
        <uri>http://www.lamaisondumeurtre.fr/instances#SalleAManger</uri>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 11
**Quelle(s) pièce(s) est/sont vide(s) ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?Piece WHERE
{
	SELECT ?Piece (COUNT(?Personne) AS ?NombrePersonnes) WHERE
	{
  		{
      		{?Personne :personneDansPiece ?Piece} UNION
      		{?Piece :pieceContientPersonne ?Personne}
    	}
	}

	GROUP BY ?Piece
	HAVING (?NombrePersonnes = 0)
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Piece"/>
  </head>
  <results>
  </results>
</sparql>
```

# Question 12
**Quels sont les suspects restants ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?Name WHERE
{
	?Personne :estVivant true.
  	?Personne rdfs:label ?Name.
  	FILTER REGEX(?Name, "(e |e$)")
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Name"/>
  </head>
  <results>
    <result>
      <binding name="Name">
        <literal xml:lang="fr">Aubin Gladèche</literal>
      </binding>
    </result>
    <result>
      <binding name="Name">
        <literal xml:lang="fr">Gustave Harié</literal>
      </binding>
    </result>
    <result>
      <binding name="Name">
        <literal xml:lang="fr">Luci Nuzyte</literal>
      </binding>
    </result>
    <result>
      <binding name="Name">
        <literal xml:lang="fr">Sophie Stulle</literal>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 13
**Qui est par conséquent innocent ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX maison: <http://www.lamaisondumeurtre.fr/instances#LaMaisonDuMeurtre>

SELECT ?Innocent WHERE
{
    maison: :maisonContientPiece ?PieceCrime.
    maison: :maisonContientPiece ?PieceInnocent.

  	{
      	# Récupération de la pièce du crime
      	?Victime :estVivant false.
  		{
      		{?Victime :personneDansPiece ?PieceCrime} UNION
      		{?PieceCrime :pieceContientPersonne ?Victime}
    	}
  	
      	# Récupération de toutes les personnes qui ne sont pas dans une pièce voisine de celle du crime
  		?Innocent :estVivant true.
  		{
      		{?Innocent :personneDansPiece ?PieceInnocent} UNION
      		{?PieceInnocent :pieceContientPersonne ?Innocent}
    	}
  		{
      		{?PieceInnocent :aPourPieceVoisine ?PieceCrime} UNION
      		{?PieceCrime :aPourPieceVoisine ?PieceInnocent}
    	}
    } #UNION
  	#{
    #  	# On innocente tous ceux dont le nom / prénom ne finit pas par 'e'
    #  	?Innocent :estVivant true.
  	#	?Innocent rdfs:label ?Name.
  	#	FILTER REGEX(?Name, "^((?!(e |e$)).)*$")
    #}
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Innocent"/>
  </head>
  <results>
    <result>
      <binding name="Innocent">
        <uri>http://www.lamaisondumeurtre.fr/instances#AubinGladeche</uri>
      </binding>
    </result>
    <result>
      <binding name="Innocent">
        <uri>http://www.lamaisondumeurtre.fr/instances#EmmaNiolia</uri>
      </binding>
    </result>
    <result>
      <binding name="Innocent">
        <uri>http://www.lamaisondumeurtre.fr/instances#GladisTileri</uri>
      </binding>
    </result>
    <result>
      <binding name="Innocent">
        <uri>http://www.lamaisondumeurtre.fr/instances#VanessaLami</uri>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 14
**Quels sont les suspects restants?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX maison: <http://www.lamaisondumeurtre.fr/instances#LaMaisonDuMeurtre>

SELECT ?Name WHERE
{
  	maison: :maisonContientPiece ?PieceCrime.
  	maison: :maisonContientPiece ?PieceSuspect.

	?Suspect :estVivant true.
  	?Suspect rdfs:label ?Name.

  	?Victime :estVivant false.
  	{
    	{?Victime :personneDansPiece ?PieceCrime} UNION
    	{?PieceCrime :pieceContientPersonne ?Victime}
    }

	# Récupération de toutes les personnes qui ne sont pas dans une pièce voisine de celle du crime
  	{
    	{?Suspect :personneDansPiece ?PieceSuspect} UNION
    	{?PieceSuspect :pieceContientPersonne ?Suspect}
    }
  	NOT EXISTS{
    	{?PieceSuspect :aPourPieceVoisine ?PieceCrime} UNION
    	{?PieceCrime :aPourPieceVoisine ?PieceSuspect}
    }

  	FILTER REGEX(?Name, "(e |e$)")
}
LIMIT 10
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Name"/>
  </head>
  <results>
    <result>
      <binding name="Name">
        <literal xml:lang="fr">Luci Nuzyte</literal>
      </binding>
    </result>
    <result>
      <binding name="Name">
        <literal xml:lang="fr">Sophie Stulle</literal>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 15
**Combien y a-t-il d'objects dans la maison ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX maison: <http://www.lamaisondumeurtre.fr/instances#LaMaisonDuMeurtre>

SELECT (count(?Object) as ?NombreObjects) WHERE
{
  	{
  		{maison: :maisonContientPiece ?Piece} UNION
   		{?Piece :pieceDansMaison maison:}
    }

  	{
  		{?Piece :pieceContientObjet ?Object} UNION
   		{?Object :objetDansPiece ?Piece}
    }
}
LIMIT 20
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="NombreObjects"/>
  </head>
  <results>
    <result>
      <binding name="NombreObjects">
        <literal datatype="http://www.w3.org/2001/XMLSchema#integer">10</literal>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 16
**Quels objets ne peuvent pas être l'arme du crime ?**

## Requête
```sparql
PREFIX : <http://www.lamaisondumeurtre.fr#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX maison: <http://www.lamaisondumeurtre.fr/instances#LaMaisonDuMeurtre>

SELECT ?Object WHERE
{
  	{
  		{maison: :maisonContientPiece ?Piece} UNION
   		{?Piece :pieceDansMaison maison:}
    }

  	{
  		{maison: :maisonContientPiece ?PieceCrime} UNION
   		{?PieceCrime :pieceDansMaison maison:}
    }

  	# Récupération de la pièce du crime
    ?Victime :estVivant false.
  	{
    	{?Victime :personneDansPiece ?PieceCrime} UNION
    	{?PieceCrime :pieceContientPersonne ?Victime}
    }

  	# Récupération de tous les objects
  	{
  		{?Piece :pieceContientObjet ?Object} UNION
   		{?Object :objetDansPiece ?Piece}
    }

  	# Suppression des objects qui sont dans la pièce du crime
  	NOT EXISTS{
  		{?PieceCrime :pieceContientObjet ?Object} UNION
   		{?Object :objetDansPiece ?PieceCrime}
    }


}
LIMIT 20
```

## Réponse
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sparql xmlns="http://www.w3.org/2005/sparql-results#">
  <head>
    <variable name="Object"/>
  </head>
  <results>
    <result>
      <binding name="Object">
        <uri>http://www.lamaisondumeurtre.fr/instances#Chandelier</uri>
      </binding>
    </result>
    <result>
      <binding name="Object">
        <uri>http://www.lamaisondumeurtre.fr/instances#Marteau</uri>
      </binding>
    </result>
    <result>
      <binding name="Object">
        <uri>http://www.lamaisondumeurtre.fr/instances#Livre</uri>
      </binding>
    </result>
    <result>
      <binding name="Object">
        <uri>http://www.lamaisondumeurtre.fr/instances#Revolver</uri>
      </binding>
    </result>
    <result>
      <binding name="Object">
        <uri>http://www.lamaisondumeurtre.fr/instances#PicAGlace</uri>
      </binding>
    </result>
    <result>
      <binding name="Object">
        <uri>http://www.lamaisondumeurtre.fr/instances#EauDeJavel</uri>
      </binding>
    </result>
    <result>
      <binding name="Object">
        <uri>http://www.lamaisondumeurtre.fr/instances#Lacet</uri>
      </binding>
    </result>
    <result>
      <binding name="Object">
        <uri>http://www.lamaisondumeurtre.fr/instances#Nokia3310</uri>
      </binding>
    </result>
  </results>
</sparql>
```

# Question 17
**Quel object est l'arme du crime ?**

