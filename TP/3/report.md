# Partie 1 : Installation d'un serveur SPARQL

[Sujet](frama.link/4aTP3)

# Partie 2 : Interrogation de la base de connaissances sur le cinéma

## Question 1
**L’ensemble des films s’étant déroulés dans un lieu ayant des coordonnées GPS :**

```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

SELECT ?nom
WHERE {
  ?film :seDerouleA ?lieu.
  ?lieu :aPourGPS ?GPS.
  ?film rdfs:label ?nom.
}
LIMIT 25
```

## Question 2
**pour chaque point (coordonnées GPS) le nombre de films et la liste des noms des films s’y étantdéroulés.**

```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

SELECT ?GPS (GROUP_CONCAT(?nom) AS ?lesFilms) (COUNT(?film) AS ?nbFilms)
WHERE {
  ?film rdf:type :OWLClass_b3ec988b_3df8_44bc_bb90_2242c83e3158.

  ?lieu :aPourGPS ?GPS.
  ?film :seDerouleA ?lieu.
  ?film rdfs:label ?nom.
}
GROUP BY ?GPS
LIMIT 25
```

## Question 3
**pour chaque point (coordonnées GPS) le nombre et la liste des noms des films POPULAIRES s’yétant déroulés**

```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

SELECT ?GPS (GROUP_CONCAT(?nom) AS ?lesFilms) (COUNT(?film) AS ?nbFilms)
WHERE {
  ?film rdf:type :OWLClass_69c537ec_d33d_47b4_8c64_a96ea951bee6.

  ?lieu :aPourGPS ?GPS.
  ?film :seDerouleA ?lieu.
  ?film rdfs:label ?nom.
}
GROUP BY ?GPS
LIMIT 25
```

## Question 4
**l’ensemble des genres qui peuvent être associés à un film.**

```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

SELECT ?nomFilm (GROUP_CONCAT(?nomGenre; separator="; ") AS ?lesGenres)
WHERE {
  ?film rdf:type :OWLClass_b3ec988b_3df8_44bc_bb90_2242c83e3158.

  ?film :aPourGenre ?genre.
  ?film rdfs:label ?nomFilm.
  ?genre rdfs:label ?nomGenre.
}
GROUP BY ?nomFilm
LIMIT 25
```

## Question 5
**pour chaque point (coordonnées GPS) le nombre et la liste des noms des films ROMANTIQUES s’y étant déroulés.**

```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

SELECT ?GPS (GROUP_CONCAT(?nom) AS ?lesFilms) (COUNT(?film) AS ?nbFilms)
WHERE {
  ?film rdf:type :OWLClass_b3ec988b_3df8_44bc_bb90_2242c83e3158.

  ?lieu :aPourGPS ?GPS.
  ?film :seDerouleA ?lieu.
  ?film rdfs:label ?nom.

  ?film :aPourGenre :Romance.
}
GROUP BY ?GPS
LIMIT 25
```

## Question 6
**les points (coordonnées GPS) où au moins 3 films se sont déroulés.**

```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

SELECT ?GPS (COUNT(?film) AS ?nbFilms)
WHERE {
  ?film rdf:type :OWLClass_b3ec988b_3df8_44bc_bb90_2242c83e3158.

  ?lieu :aPourGPS ?GPS.
  ?film :seDerouleA ?lieu.
}
GROUP BY ?GPS
HAVING(?nbFilms >= 3)
LIMIT 25
```

# Partie 3 : Mise à jour de la base de connaissances

## Question 2
**Faire une requête SPARQL vérifiant si Montpellier a une coordonnée GPS.**

```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

ASK {
  :MONTPELLIER :aPourGPS ?GPS.
}
```

## Question 3
**Mettez à jour la base de connaissances afin d’indiquer que lescoordonnées GPS de Montpellier sont43.61092,3.87723**

* Update
```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX peup: <http://www.semanticweb.org/nathalie/ontologies/cinemaTPpeup#>

INSERT DATA {
  peup:MONTPELLIER :aPourGPS "43.61092,3.87723".
}
```

* Query
```sparql
PREFIX : <http://www.semanticweb.org/nathalie/ontologies/cinemaTP#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX peup: <http://www.semanticweb.org/nathalie/ontologies/cinemaTPpeup#>

SELECT ?GPS
WHERE {
  peup:MONTPELLIER :aPourGPS ?GPS.
}
LIMIT 25
```
